import gym.spaces
import matplotlib.pyplot as plt
import numpy as np
from qlearning_template import QLearningAgent
from sarsa_template import SarsaAgent
from ev_sarsa_template import EVSarsaAgent
from cliff_walking import CliffWalkingEnv

def play_and_train(env, agent, t_max=10 ** 4):
    """ This function should
    - run a full game (for t_max steps), actions given by agent
    - train agent whenever possible
    - return total reward
    """
    total_reward = 0.0
    state = env.reset()
    for t in range(t_max):
        action = agent.get_action(state)
        new_state, reward, is_done, _ = env.step(action)
        agent.update(state, action, new_state, reward)
        total_reward += reward
        state = new_state
        if is_done:
            break

    return total_reward


def update_epsilon(model, alpha=0.99, t=0.1):
    if model.epsilon > t:
        model.epsilon *= alpha


if __name__ == '__main__':
    max_iterations = 1000
    visualize = True
    # Create Taxi-v2 env
    # env = gym.make('Taxi-v2').env
    env = CliffWalkingEnv()
    env.reset()
    env.render()

    n_states = env.nS
    n_actions = env.nA

    print('States number = %i, Actions number = %i' % (n_states, n_actions))

    # create q learning agent with
    # alpha=0.5
    # get_legal_actions = lambda s: range(n_actions)
    # epsilon=0.1
    # discount=0.99

    sarsa_agent = SarsaAgent(alpha=0.5, epsilon=0.1, discount=0.99, get_legal_actions=lambda s: range(n_actions))
    ql_agent = QLearningAgent(alpha=0.5, epsilon=0.1, discount=0.99, get_legal_actions=lambda s: range(n_actions))
    evsarsa_agent = EVSarsaAgent(alpha=0.5, epsilon=0.1, discount=0.99, get_legal_actions=lambda s: range(n_actions))

    plt.figure(figsize=[10, 4])
    sarsa_rewards = []
    evsarsa_rewards = []
    ql_rewards = []

    # Training loop
    for i in range(max_iterations):
        sarsa_rewards.append(play_and_train(env, sarsa_agent))
        ql_rewards.append(play_and_train(env, ql_agent))
        evsarsa_rewards.append(play_and_train(env, evsarsa_agent))

        # Play & train game
        # Update rewards
        # rewards

        # Decay agent epsilon
        # agent.epsilon = ?
        update_epsilon(ql_agent)
        update_epsilon(sarsa_agent)
        update_epsilon(evsarsa_agent)

        if i % 100 == 0:
            print('QL:  Iteration {}, Average reward {:.2f}, Epsilon {:.3f}'.format(i, np.mean(ql_rewards), ql_agent.epsilon))
            print('SARSA:  Iteration {}, Average reward {:.2f}, Epsilon {:.3f}'.format(i, np.mean(sarsa_rewards), sarsa_agent.epsilon))
            print('EV-SARSA:  Iteration {}, Average reward {:.2f}, Epsilon {:.3f}'.format(i, np.mean(evsarsa_rewards), evsarsa_agent.epsilon))


        if visualize:
            plt.subplot(1, 2, 1)
            plt.plot(ql_rewards, color='blue')
            plt.plot(sarsa_rewards, color='red')
            plt.plot(evsarsa_rewards, color='green')
            plt.xlabel('Iterations')
            plt.ylabel('Total Reward')

            plt.subplot(1, 2, 2)
            plt.hist(ql_rewards, bins=20, range=[-700, +20], color='blue', label='QL rewards distribution')
            plt.hist(sarsa_rewards, bins=20, range=[-700, +20], color='red', label='SARSA rewards distribution')
            plt.hist(evsarsa_rewards, bins=20, range=[-700, +20], color='green', label='EV-SARSA rewards distribution')
            plt.xlabel('Reward')
            plt.ylabel('p(Reward)')
            plt.draw()
            plt.pause(0.05)
            plt.cla()
